import math
import random

from renpy.easy import displayable
from renpy.rollback import NoRollback
from renpy import config

from renpy.display.particle import Particles


class AuraParticleFactory(NoRollback):

    def __setstate__(self, state):
        self.start = 0
        vars(self).update(state)
        self.init()

    def __init__(self, image, count, offset, image_size, minspeed, speed, spread, start, fast):
        self.image = displayable(image)
        self.count = count
        self.offset = offset
        self.image_size = image_size
        self.minspeed = minspeed
        self.speed = speed
        self.spread = spread
        self.start = start
        self.fast = fast
        self.init()

    def init(self):
        self.starts = [random.uniform(0, self.start) for _i in range(0, self.count)]
        self.starts.append(self.start)
        self.starts.sort()

    def create(self, particles, st):

        def ranged(n):
            if isinstance(n, tuple):
                rand_speed = random.uniform(n[0], n[1])
                if abs(rand_speed) < self.minspeed:
                    return random.choice([self.minspeed, -self.minspeed])
                return rand_speed
            else:
                return n

        if (st == 0) and not particles and self.fast:
            particles_list = []

            for _i in range(0, self.count):
                particles_list.append(
                    AuraParticle(
                        self.image,
                        self.offset,
                        self.image_size,
                        self.minspeed,
                        ranged(self.speed),
                        ranged(self.speed),
                        self.spread,
                        st,
                    ))
            return particles_list

        if particles is None or len(particles) < self.count:

            # Check to see if we have a particle ready to start. If not,
            # don't start it.
            if particles and st < self.starts[len(particles)]:
                return None

            return [
                AuraParticle(
                    self.image,
                    self.offset,
                    self.image_size,
                    self.minspeed,
                    ranged(self.speed),
                    ranged(self.speed),
                    self.spread,
                    st,
                )]

    def predict(self):
        return [self.image]


class AuraParticle(NoRollback):
    def __init__(self, image, offset, image_size, minspeed, xspeed, yspeed, spread, start):

        # safety.
        if yspeed == 0:
            yspeed = 1

        self.image = image
        self.minspeed = minspeed
        self.xspeed = xspeed
        self.yspeed = yspeed
        self.spread = spread
        self.start = start
        self.offset = offset
        self.image_size = image_size

        x_or_y = random.choice(["x", "y"])
        spread_x = random.randint(0, spread * config.screen_width)
        spread_y = random.randint(0, spread * config.screen_height)

        rand_x = random.choice([spread_x, config.screen_width - spread_x])
        rand_y = random.choice([spread_y, config.screen_height - spread_y])

        if x_or_y == "x":
            self.xstart = random.randint(0, config.screen_width)
            self.ystart = random.choice([rand_y, config.screen_height])
        if x_or_y == "y":
            self.xstart = random.choice([rand_x, config.screen_width])
            self.ystart = random.randint(0, config.screen_height)

        self.xstart = self.xstart - int(self.image_size / 2)
        self.ystart = self.ystart - int(self.image_size / 2)

    def update(self, st):
        finish = st - self.start

        xpos = self.xstart + finish * self.xspeed
        ypos = self.ystart + finish * self.yspeed

        if xpos > config.screen_width or ypos > config.screen_height:
            return None
        elif xpos < -self.image_size or ypos < -self.image_size:
            return None

        diff_x = xpos - self.xstart
        diff_y = ypos - self.ystart

        distance = math.sqrt(math.pow(diff_x, 2) + math.pow(diff_y, 2))

        # we dont want them moving very far before respawning
        if distance > 100:
            return None

        return int(xpos), int(ypos), finish + self.offset, self.image


def aura_generator(
    d,
    offset=0,
    image_size=100,
    spread=100,
    count=10,
    minspeed=100,
    speed=(-50, 50),
    start=10,
    fast=False,
):

    return Particles(
        AuraParticleFactory(
            image=d,
            offset=offset,
            image_size=image_size,
            spread=spread,
            count=count,
            minspeed=minspeed,
            speed=speed,
            start=start,
            fast=fast,
        ))
