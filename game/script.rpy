﻿
image grey = Solid("#555")
image blue = Solid("#246786")

label start:

    scene grey

    show particles

    "White particles."

    hide particles
    show particles_red

    "Red particles."

    hide particles_red
    show particles_black

    "Black particles."

    hide particles_black
    scene blue
    show particles_more

    "Blue particles on blue background - could be used for a water effect."

    return
