
# Particles

init -2 python:
    # put aura_particle.py in the same directory as this file
    # then import the function aura_generator from it with this line
    from aura_particle import aura_generator


image particle_soft:
    "big_particle"
    size (1440, 1440)  # make it BIG
    additive True  # turn on additive blending
    alpha 0.0  # give the image a nice fade in/out
    ease 0.2 alpha 1.0
    alpha 1.0
    ease 2.0 alpha 0.0

image particle_blue:
    "particle_soft"  # reuse the above image and tweak the colour
    matrixcolor ColorizeMatrix("#eff", "#eff")

image particle_red:
    "particle_soft"
    # add some random shades to choose from
    choice:
        matrixcolor ColorizeMatrix("#ff0000", "#ff0000")
    choice:
        matrixcolor ColorizeMatrix("#bc0000", "#bc0000")
    choice:
        matrixcolor ColorizeMatrix("#880000", "#880000")

image particle_black:
    "big_particle"
    matrixcolor ColorizeMatrix("#000", "#000")
    size (1440, 1440)  # make it BIG
    alpha 0.0  # give the image a nice fade in/out
    ease 0.2 alpha 1.0  # .2 sec fade in
    alpha 1.0
    ease 2.0 alpha 0.0  # 2 sec fadeout

image particles = aura_generator(
    "particle_soft",  # source image ideally should be square
    spread=0.1,  # 0 means particles only spawn at the very edge of the screen. 1 means they will spawn between the edge and middle
    count=50,  # how many particles on screen at once
    image_size=1440,  # should match the size of your image. used for positioning
    start=10,  # delay between new particles spawning
    minspeed=10,  # minimum speed of a particle
    speed=(-50, 50))  # range of speeds. negative and positive value means they will go left/right and up/down

image particles_red = aura_generator(
    "particle_red",
    spread=0.1,
    count=50,
    image_size=1440,
    start=10,
    minspeed=10,
    speed=(-50, 50))

image particles_black = aura_generator(
    "particle_black",
    spread=0.1,
    count=50,
    image_size=1440,
    start=10,
    minspeed=10,
    speed=(-50, 50))

image particles_more = aura_generator(
    "particle_blue",
    spread=1.0,
    count=300,
    image_size=1440,
    start=10,
    minspeed=10,
    speed=(-50, 50))
